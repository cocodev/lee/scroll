    include scroll_inc.asm

_initializeDiskIo           export
saveErrorHandlerStack       export
setDiskErrorHandler         export
clearDiskErrorHandler       export
fileOpenCloseErrorHandler   export
fileReadErrorHandler        export
closeAllFiles               export
openFileForRead             export
readByteFromFile            export
readBytesFromFileIntoBuffer export
copyFilenameToBuffer        export
bytesToRead                 export
bytesReadBuffer             export

testFilename                import

openFileForInput            rmb 2
openFileForOutput           rmb 2
diskIrq                     rmb 2
originalErrorHandlerOpCode  rmb 1
originalErrorHandler        rmb 2
errorHandlerStack           rmb 2

    section   code
    pragma  newsource

bytesToRead                 rmb 2
bytesReadBuffer             rmb 2

;***********************
;** _initializeDiskIo **
;***********************
_initializeDiskIo
    pshs    u,y,x
    ldx     $C004               ; Get DSKCON address pointer
    cmpx    #$D75F              ; Is it Disk Basic 1.1?
    beq     @disk_11
    cmpx    #$d66c              ; Is it Disk Basic 1.0?
    beq     @disk_10
    ldx     #0
    ldu     #0
    ldy     #0
    bra     @set_routines
@disk_10
    ldx     #$C956              ; address of "open system file for output" routine
    ldu     #$C959              ; address of "open system file for input" routine
    ldy     #$D7C4              ; point to Disk IRQ handler
    bra @set_routines
@disk_11
    ldx     #$CA04              ; address of "open system file for output" routine
    ldu     #$CA07              ; address of "Open system file for input" routine
    ldy     #$D8B7              ; point to Disk IRQ handler
@set_routines
    stx     openFileForOutput,pcr  ; save address of "open for output"
    stu     openFileForInput,pcr   ; save address of "open for input"
    sty     diskIrq,pcr         ; save IRQ hook for Basic
    bne     @no_error
    clrb
    bra     @done
@no_error
    ldb     #1
@done
    clra
    puls    x,y,u,pc

;***************************
;** saveErrorHandlerStack **
;***************************
saveErrorHandlerStack
    puls    y
    sts     errorHandlerStack,pcr
    jmp     ,y

;**************************
;** setDiskErrorHandler  **
;**                      **
;** >> x = error handler **
;** << a,y = trashed     **
;** << cc = set on error **
;**************************
setDiskErrorHandler
    pshs    a,y
    lda     ErrorHandlerVector
    ldy     ErrorHandlerVector+1
    sta     originalErrorHandlerOpCode,pcr
    sty     originalErrorHandler,pcr
    lda     #$7E        ; JMP
    sta     ErrorHandlerVector
    stx     ErrorHandlerVector+1
    puls    y,a,pc

;***************************
;** clearDiskErrorHandler **
;***************************
clearDiskErrorHandler
    pshs    x,a
    lda     originalErrorHandlerOpCode,pcr
    ldx     originalErrorHandler,pcr
    sta     ErrorHandlerVector
    stx     ErrorHandlerVector+1
    puls    a,x,pc

;*******************************
;** fileOpenCloseErrorHandler **
;*******************************
fileOpenCloseErrorHandler
    lds     errorHandlerStack       ; Restore stack to when handler was set
    bsr     clearDiskErrorHandler
    coma                            ; set cc flag to indicate error
    rts                             ; Returns from the function that set the trap

;**************************
;** fileReadErrorHandler **
;**************************
fileReadErrorHandler
    lds     errorHandlerStack       ; Restore stack to when handler was set
    bsr     clearDiskErrorHandler
    bsr     closeAllFiles
    coma                            ; set cc flag to indicate error
    rts                             ; Returns from the function that set the trap

;**************************
;** closeAllFiles        **
;**                      **
;** << cc = set on error **
;**************************
closeAllFiles
    bsr     saveErrorHandlerStack
    leax    fileOpenCloseErrorHandler,pcr
    bsr     setDiskErrorHandler
    jsr     $A426
    jsr     clearDiskErrorHandler
    clra
    rts

;****************************************************
;** openFileForRead                                **
;**                                                **
;** >> x = address of 8.3 filename, \0 terminated. **
;****************************************************
openFileForRead
    bsr     saveErrorHandlerStack
    ;
    jsr     copyFilenameToBuffer
    leax    fileOpenCloseErrorHandler,pcr
    jsr     setDiskErrorHandler
    ;
    jsr     [openFileForInput]          ; Open the file
    ;
    jsr     clearDiskErrorHandler       ; Clear error handler
    clra
    rts

;**************************
;** readByteFromFile     **
;**                      **
;** << a  = byte read    **
;** << cc = set on error **
;**************************
readByteFromFile
    jsr     saveErrorHandlerStack
    pshs    x,b
    leax    fileReadErrorHandler,pcr
    jsr     setDiskErrorHandler
    ;
    jsr     ConsoleIn
    ;
    jsr     clearDiskErrorHandler
    clrb                                ; Clear cc to indicate no error
    puls    b,x,pc

;***************************************************************
;** readBytesFromFileIntoBuffer                               **
;**                                                           **
;** >> bytesToRead     = Number of bytes to read              **
;** >> bytesReadbuffer = Address of buffer to read bytes into **
;** << x   = number of bytes actually read                    **
;** << cc  = set if error                                     **
;***************************************************************
readBytesFromFileIntoBuffer
    lbsr     saveErrorHandlerStack
    leax    fileReadErrorHandler,pcr
    lbsr     setDiskErrorHandler
    ldy     bytesReadBuffer,pcr
    ldx     #0
@readBytes
    cmpx    bytesToRead,pcr       ; Have we read the request # of bytes?
    beq     @done                 ; If so, done with no error
    jsr     ConsoleIn             ; Read a byte from the file
    tst     ConsoleInBufferFlag   ; Did we hit EOF?
    bne     @done                 ; If so, done with no error
    sta     ,y+                   ; Store byte read into buffer
    leax    1,x                   ; Increment number of bytes read
    bra     @readBytes            ; Loop back for more
@done
    lbsr clearDiskErrorHandler
    clra
    rts

;****************************************************
;** copyFilenameToBuffer                           **
;**                                                **
;** >> x = address of 8.3 filename, \0 terminated. **
;****************************************************
copyFilenameToBuffer
    ; Clear filename buffer
    ldy     #FileNameBuffer
    lda     #$20
    ldb     #11
@clearFilename
    sta     ,y+
    decb
    bne     @clearFilename
    ;
@loadFilename
    ldy     #FileNameBuffer
@filenameLoop
    lda     ,x+
    beq     @doneFilename
    cmpa    #46             ; .
    beq     @skipDot
    cmpy    #FileExtensionBuffer
    beq     @loadExtension
    sta     ,y+
    bra     @filenameLoop
@skipDot
    leay    1,y
@loadExtension
    ldy     #FileExtensionBuffer
@extensionLoop
    lda     ,x+
    beq     @doneFilename
    cmpy    #FileExtensionBuffer+3
    beq     @doneFilename
    sta     ,y+
    bra     @extensionLoop
@doneFilename
    rts

    endsection
