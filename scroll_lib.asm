    include scroll_inc.asm

gimeRegister0Value              export
Set80x24TextMode                export
DisableInterruptSources         export
RestoreInterruptSources         export
SwapRomsOut                     export
SwapRomsBackIn                  export
PrintChar80x24                  export
UpdateRomCursor                 export
_c_PrintString                  export
PerformScrollTest               export
InterruptHandler                export
_IsKeyDownRom                   export
_IsKeyDownNoRom                 export
_SetSafeMmuScratchBlock         export
_GetSafeMmuScratchBlock         export
_GetSafeMmuScratchBlockAddress  export
    section code
    pragma  undefextern
    pragma  newsource

safeMmuLogicalBlock             fcb LOGICAL_4000_5FFF ; MMU block to use temporarily to access physical blocks
safeMmuLogicalBlockAddress      fdb LOGICAL_4000_5FFF * $2000
printChar80x24_CurrentCursorX   fcb 0
printChar80x24_CurrentCursorY   fcb 0
gimeRegister0Value              fcb %01001110         ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                                      ; 1  = MMU enabled (0 = disabled)
                                                      ; 0  = GIME IRQ disabled (1 = enabled)
                                                      ; 0  = GIME FIRQ disabled (1 = enabled)
                                                      ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                                      ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                                      ; 10 = ROM Map 32k Internal
                                                      ;      0x = 16K Internal, 16K External
                                                      ;      11 = 32K External - Except Interrupt Vectors
paletteStore                    rmb 16
paletteCga                      fcb %000000 ; 0
                                fcb %001000 ; 1
                                fcb %010000 ; 2
                                fcb %011000 ; 3
                                fcb %100000 ; 4
                                fcb %101000 ; 5
                                fcb %100010 ; 6
                                fcb %111000 ; 7
                                fcb %000111 ; 8
                                fcb %001011 ; 9
                                fcb %010111 ; 10
                                fcb %011011 ; 11
                                fcb %100100 ; 12
                                fcb %101101 ; 13
                                fcb %110110 ; 14
                                fcb %111111 ; 15
keyColumnScans                  fcb 1,1,1,1,1,1,1,1

;***********************
Set80x24TextMode
;***********************
    lda     #%00000011                  ; VideoMode        - BP(7), Unused(6), DESCEN(5), MOCH(4), H50(3), LPR2-LPR0=(2-0)
    sta     $FF98
    lda     #%00010101                  ; VideoResolution  - Unused(7), VRES1-VRES0(6-5), HRES2-HRES0(4-2), CRES1-CRES0(1-0)
    sta     $FF99
    lda     #%00010010                  ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     $FF9A
    lda     #%00000000                  ; VerticalScroll   - Unused=(7-4), SCEN(3), SC2-SC0(2-0)
    sta     $FF9C
    lda     #%11011000                  ; VerticalOffset1  - Physical address bits 18-3=0b1101100000000000000 (bits 2-0 always 0) == 0x6C000
    std     $FF9D
    lda     #%00000000                  ; VerticalOffset0  - 
    std     $FF9E
    lda     #%00000000                  ; HorizontalOffset - HE(6), X6-X0(6-0)
    sta     $FF9F
    lda     #%01001110                  ; CoCo 3 Mode, MMU Enabled, GIME IRQ disabled, GIME FIRQ disabled, Vector RAM at
    sta     gimeRegister0Value
    sta     $FF90                       ; $FEXX enabled, Standard SCS Normal, ROM Map 32k Internal
    rts

;    Name                     BitPlane          LPR    VRES  HRES   CRES  BPP  Width Height
; { "Graphics 320x200x16",    BitPlaneGraphics, 0b001, 0b01, 0b111, 0b10,   4, 320/2,  200 },
;***********************
Set320x200x16ScrollableGraphicsMode
;***********************
    lda     #%10000001                  ; VideoMode        - BP(7), Unused(6), DESCEN(5), MOCH(4), H50(3), LPR2-LPR0=(2-0)
    sta     $FF98
    lda     #%00111110                  ; VideoResolution  - Unused(7), VRES1-VRES0(6-5), HRES2-HRES0(4-2), CRES1-CRES0(1-0)
    sta     $FF99
    lda     #%00000000                  ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     $FF9A
    lda     #%00000000                  ; VerticalScroll   - Unused=(7-4), SCEN(3), SC2-SC0(2-0)
    sta     $FF9C
    lda     #%00000000                  ; VerticalOffset1  - Physical address bits 18-11
    std     $FF9D                       ;                                                = 0b00000000 00000000 000 (bits 2-0 always 0) == 0x00000
    lda     #%00000000                  ; VerticalOffset0  - Physical address bits 10-03
    std     $FF9E
    lda     #%10000000                  ; HorizontalOffset - HE(6), X6-X0(6-0)
    sta     $FF9F
    lda     #%01001110                  ; CoCo 3 Mode, MMU Enabled, GIME IRQ disabled, GIME FIRQ disabled, Vector RAM at
    sta     gimeRegister0Value
    sta     $FF90                       ; $FEXX enabled, Standard SCS Normal, ROM Map 32k Internal
    rts

;***********************
DisableInterruptSources
;***********************
    lda     $FF01
    anda    #$FE
    sta     $FF01                       ; Disable HSYNC IRQ

    lda     $FF03
    ora     #$01
    sta     $FF03                       ; Enable VSYNC IRQ

    lda     $FF21
    anda    #$FE
    sta     $FF21                       ; Disable Cassette Data FIRQ

    lda     $FF23
    anda    #$FE
    sta     $FF23                       ; Disable Cartridge FIRQ

    lda     #%01001110
    sta     gimeRegister0Value
    sta     $FF90                       ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                        ; 1  = MMU enabled (0 = disabled)
                                        ; 0  = GIME IRQ disabled (1 = enabled)
                                        ; 0  = GIME FIRQ disabled (1 = enabled)
                                        ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                        ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                        ; 10 = ROM Map 32k Internal
                                        ;      0x = 16K Internal, 16K External
                                        ;      11 = 32K External - Except Interrupt Vectors

    lda     #%00000000
    sta     $FF92                       ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     $FF93                       ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    rts

;***********************
RestoreInterruptSources
;***********************
    lda     $FF03
    ora     #$01
    sta     $FF03                       ; Enable VSYNC IRQ

    lda     $FF01
    anda    #$FE
    sta     $FF01                       ; Disable HSYNC IRQ

    lda     $FF21
    anda    #$FE
    sta     $FF21                       ; Disable the Cassette Data FIRQ

    lda     $FF23
    anda    #$FE
    sta     $FF23                       ; Disable the Cartridge FIRQ

    lda     #%01001110
    sta     $FF90                       ; 0  = CoCo 3 Mode, 1  = CoCo 1/2 Compatible, 
                                        ; 1  = MMU enabled (0 = disabled)
                                        ; 0  = GIME IRQ disabled (1 = enabled)
                                        ; 0  = GIME FIRQ disabled (1 = enabled)
                                        ; 1  = Vector RAM at $FEXX enabled (0 = disabled)
                                        ; 1  = Standard SCS (DISK) Normal (0 = expand)
                                        ; 10 = ROM Map 32k Internal
                                        ;      0x = 16K Internal, 16K External
                                        ;      11 = 32K External - Except Interrupt Vectors

    lda     #%00000000
    sta     $FF92                       ; Disable IRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    sta     $FF93                       ; Disable FIRQs: Timer, HSYNC, VSYNC, RS232, Keyboard, Cartridge
    rts

;***********************
SwapRomsOut
;***********************
    ldb     #PHYSICAL_60000_61FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    _MMU+LOGICAL_0000_1FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_68000_69FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    _MMU+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_6A000_6BFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    _MMU+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_6C000_6DFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    _MMU+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_6E000_6FFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    _MMU+LOGICAL_E000_FFFF,pcr
    stb     ,x
    ;
    ldb     #LOGICAL_8000_9FFF
    stb     safeMmuLogicalBlock,pcr
    rts

;***********************
SwapRomsBackIn
;***********************
    ldb     #PHYSICAL_70000_71FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_0000_1FFF
    leax    _MMU+LOGICAL_0000_1FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_78000_79FFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_8000_9FFF
    leax    _MMU+LOGICAL_8000_9FFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_7A000_7BFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_A000_BFFF
    leax    _MMU+LOGICAL_A000_BFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_7C000_7DFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_C000_DFFF
    leax    _MMU+LOGICAL_C000_DFFF,pcr
    stb     ,x
    ;
    ldb     #PHYSICAL_7E000_7FFFF
    stb     MMU_BANK_REGISTERS_FIRST+LOGICAL_E000_FFFF
    leax    _MMU+LOGICAL_E000_FFFF,pcr
    stb     ,x

    ldb     #LOGICAL_4000_5FFF
    stb     safeMmuLogicalBlock,pcr

    lbsr    UpdateRomCursor

    rts

;***********************
_c_PrintString
;***********************
    pshs    x
    ldx     4,s
    bsr     PrintString
    puls    x,pc

; > x = pointer to string to print (zero terminated)
;***********************
PrintString
;***********************
    pshs    x,a
@next
    lda     ,x+
    beq     @done
    bsr     PrintChar80x24
    bra     @next
@done
    puls    a,x
    rts

;***********************
PrintChar80x24
;***********************
    pshs    x,b,a
    ; Map in $6C000 to our safeMmuLogicalBlock
    ldb     safeMmuLogicalBlock,pcr
    ldx     #MMU_BANK_REGISTERS_FIRST
    abx
    lda     #PHYSICAL_6C000_6DFFF
    sta     ,x                          ; Logical Block safeMmuLogicalBlock is now mapped to physical block $6C000
    leax    _MMU,pcr
    abx
    ldb     ,x                          ; b = original physical bock mapped to safeMmuLogicalBlock
    ; Calculate the logical address for the next character
    lda     safeMmuLogicalBlock,pcr
    ldx     #$0000
@multiply
    tsta
    beq     @doneMultiply
    leax    $2000,x
    deca
    bra     @multiply
@doneMultiply
    lda     printChar80x24_CurrentCursorY,pcr
@adjustForY
    tsta
    beq     @doneAdjustForY
    leax    160,x
    deca
    bra     @adjustForY
@doneAdjustForY
    puls    a                           ; Get the character to print back into a
    pshs    b
    ldb     printChar80x24_CurrentCursorX,pcr
    abx
    abx
    ; Print the character
    cmpa    #13                         ; Newline?
    bne     @not_newline
    ; Print a newline (blank rest of line, move cursor down one and all the way to the left)
    lda     printChar80x24_CurrentCursorX,pcr
    ldb     #32                         ; space
@blankRestOfLine
    tsta
    beq     @doneBlankingLine
    stb     ,x+
    clr     ,x+
    deca
    bra     @blankRestOfLine
@doneBlankingLine
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
    bra     @donePrint
    ; Print the character
@not_newline
    sta     ,x+
    clr     ,x+
    inc     printChar80x24_CurrentCursorX,pcr ; Move one character to the right
    ;
@donePrint
    lda     printChar80x24_CurrentCursorX,pcr
    cmpa    #80                               ; Did we just print in the right-most column?
    blt     @doneAdjustX
    inc     printChar80x24_CurrentCursorY,pcr ; Move down one line
    clr     printChar80x24_CurrentCursorX,pcr ; Move back to beginning of line
@doneAdjustX
    lda     printChar80x24_CurrentCursorY,pcr
    cmpa    #24                               ; Did we just move off the bottom of the screen?
    blt     @doneAdjustY
    clr     printChar80x24_CurrentCursorY,pcr ; Move back to the top of the screen
@doneAdjustY
    ; Map the original physical block back into our safeMmuLogicalBlock
    ldb     safeMmuLogicalBlock,pcr
    ldx     #MMU_BANK_REGISTERS_FIRST
    abx
    puls    b
    stb     ,x
    ; Character printed.  We're out'a here!
    puls    b,x,pc

;***********************
UpdateRomCursor:
;***********************
    ldd     printChar80x24_CurrentCursorX   ;// Load x into a and y into b
    incb                                    ;// y is 1 based for ROMS, 0 based for us
    std     $FE02                           ;// Store x at $FE02 and y at $FE03
    pshs    a                               ;// Push x
    lda     #160
    mul                                     ;// d = 160 * y
    addb    ,s+                             ;// d += x
    adca    #0
    addd    #$2000                          ;// d += $2000
    std     $FE00                           ;// Store address of cursor at $FE00
    rts

; Set EGA 16 color pallete
; Set 320x200x16 video mode
;  - 256 byte wide (only 160 bytes wide)
;  - Starting at physical $00000
; Draw pattern 512 x 392 (100,352 bytes, 13 blocks, 00000-19FFF, actually 00000-18800)
; 1. Scroll minimal amount horizontally on VSYNC, bounce back and forth
; 2. Scroll right, then down, then left, then up on VSYNC, repeat
; 3. Scroll based on arrow keys pressed
; 4. Scroll based on joystick
horizontalScroll            fcb     0
nextPixel                   fcb     0
nextPhysicalBlock           fcb     0
addressFirstByteInSafeBlock rmb     2
addressFirstByteInNextBlock rmb     2
shouldScrollUp              fcb     0
shouldScrollDown            fcb     0
shouldScrollLeft            fcb     0
shouldScrollRight           fcb     0
shouldQuit                  fcb     0
;***********************
PerformScrollTest:
;***********************
    pshs    y,x,b,a
    pshsw
    ;
    lbsr    SavePalette
    lbsr    SetCgaPalette
    lbsr    Set320x200x16ScrollableGraphicsMode
    ;
    ; Map physical blocks 0 ($00000-$01FFF) to our safeMmuLogicalBlock
    lda     #PHYSICAL_00000_01FFF
    lbsr    mapPhysicalBlockToSafeMmuLogicalBlock
    pshs    a
    ;
    ldx     safeMmuLogicalBlockAddress,pcr
    ;
    ; Store 2 lines (256 bytes each) of each of 16 colors (256 * 2 *16 = $$2000) in the safeMmuLogicalBlock block (physical $00000-$01FFF)
    lde     #16
    ldy     #$0000
@nextlines
    ldd     #128*2
@drawlines
    sty     ,x++
    decd
    bne     @drawlines
    ;
    leay    $1111,y
    dece
    bne     @nextlines
    ; Map the original physical block back into our safeMmuLogicalBlock
    ldb     safeMmuLogicalBlock,pcr
    ldx     #MMU_BANK_REGISTERS_FIRST
    abx
    puls    b
    stb     ,x
    ;
@startLoop
    lda     #%00100000      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     $FF9A
    ;
    lbsr    clearGameKeys
    leax    keyColumnScans,pcr
    ldb     #1
    ;
    lda     3,x
    anda    #%00001000
    beq     @doneUpArrow
    stb     shouldScrollUp,pcr
@doneUpArrow
    lda     4,x
    anda    #%00001000
    beq     @doneDownArrow
    stb     shouldScrollDown,pcr
@doneDownArrow
    lda     5,x
    anda    #%00001000
    beq     @doneLeftArrow
    stb     shouldScrollLeft,pcr
@doneLeftArrow
    lda     6,x
    anda    #%00001000
    beq     @doneRightArrow
    stb     shouldScrollRight,pcr
@doneRightArrow
    lda     7,x
    anda    #%00001000
    beq     @doneSpace
    stb     shouldQuit,pcr
@doneSpace
    ;
    ; Map physical block $00000-$01FFF to safeMmuLogicalBlock
    lda     #PHYSICAL_00000_01FFF
    sta     nextPhysicalBlock,pcr
    lbsr    mapPhysicalBlockToSafeMmuLogicalBlock
    pshs    a
    ; Calculate address of first byte of new column of pixels in
    clra
    ldb     horizontalScroll,pcr
    lslb
    tst     shouldScrollRight,pcr
    beq     @noScrollRight
    addb    #160
    bra     @doneScrollCalculation
@noScrollRight
    tst     shouldScrollLeft,pcr
    beq     @doneScroll
    addb    #-2
@doneScrollCalculation
    addd    safeMmuLogicalBlockAddress,pcr
    tfr     d,x
    leax    $2000,x
    stx     addressFirstByteInNextBlock,pcr
    leax    -$2000,x
    stx     addressFirstByteInSafeBlock,pcr
    ; Calculate two bytes (4 pixels) worth of the next 4 colors
    cmpx    safeMmuLogicalBlockAddress,pcr
    bne     @calculateNextColor
    ldd     #$FFFF
    bra     @doneCalculateNextColor
@calculateNextColor
    lda     nextPixel,pcr
    lsla
    lsla
    lsla
    lsla
    adda    nextPixel,pcr
    inca                    ; a now has nextPixel in the upper 4 bits and nextPixel + 1 in the lower 4 bits
    tfr     a,b
    addb    #$22            ; b now has nextPixel + 2 in the upper 4 bits and nextPixel + 3 in the lower 4 bits
@doneCalculateNextColor
    ; store 4 new pixels (2 bytes) in a column starting at offset for 32 rows
    ldw     #0
    ldx     addressFirstByteInNextBlock,pcr
@loop_drawColumn
    cmpx    addressFirstByteInNextBlock,pcr
    blo     @blockMapped
    pshs    a
    lda     nextPhysicalBlock,pcr
    inc     nextPhysicalBlock,pcr
    lbsr    mapPhysicalBlockToSafeMmuLogicalBlock
    puls    a
    ldx     addressFirstByteInSafeBlock,pcr
@blockMapped
    std     ,x
    leax    256,x
    cmpd    #$FFFF
    beq     @doneAdjustPixels
    addd    #$1111
@doneAdjustPixels
    incw
    cmpw    #512
    bne     @loop_drawColumn
    ; Next colors
    lda     nextPixel,pcr
    adda    #4
    cmpa    #16
    bne     @done_nextPixel
    clra
@done_nextPixel
    sta     nextPixel,pcr
@doneScroll
    ; Restore original physical block mapped to safeMmuLogicalBlock
    puls    a
    lbsr    mapPhysicalBlockToSafeMmuLogicalBlock
    ;
    lda     #%00000000      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     $FF9A
    ; Incement the horiontal scroll by 1 (scrolling to the right 2 bytes)
    lbsr    TriggerAndWaitForScroll
    tst     shouldQuit,pcr
    lbeq    @startLoop
    ;
    bsr     clearGameKeys
    lbsr    RestorePalette
    lbsr    Set80x24TextMode
    ;
    pulsw
    puls    a,b,x,y,pc

;***********************
clearGameKeys
;***********************
    pshs    a
    clra
    sta     shouldScrollUp,pcr
    sta     shouldScrollDown,pcr
    sta     shouldScrollLeft,pcr
    sta     shouldScrollRight,pcr
    sta     shouldQuit,pcr
    puls    a,pc

;***********************
SetCgaPalette:
;***********************
    pshs    y,x,b,a
    ;
    ; Store the current pallete values for later restoring
    ldy     #$FFB0
    leax    paletteStore,pcr
    ldb     #15
@loop_store
    lda     b,y
    sta     b,x
    tstb
    beq     @done_store
    decb
    bra     @loop_store
@done_store
    ;
    ; Set the pallete
    leax    paletteCga,pcr
    ldb     #15
@loop_set
    lda     b,x
    sta     b,y
    tstb
    beq     @done_set
    decb
    bra     @loop_set
@done_set
    ;
    puls    a,b,x,y,pc

;***********************
SavePalette:
;***********************
    pshs    y,x,b,a
    ;
    ldy     #$FFB0
    leax    paletteStore,pcr
    ldb     #15
@loop
    lda     b,y
    sta     b,x
    tstb
    beq     @done
    decb
    bra     @loop
@done
    ;
    puls    a,b,x,y,pc

;***********************
RestorePalette:
;***********************
    pshs    y,x,b,a
    ;
    ldy     #$FFB0
    leax    paletteStore,pcr
    ldb     #15
@loop
    lda     b,x
    sta     b,y
    tstb
    beq     @done
    decb
    bra     @loop
@done
    ;
    puls    a,b,x,y,pc

triggerScroll   fcb     0
;***********************
InterruptHandler:
;***********************
    tst     $FF03
    lbpl    @doneVsync
    ;
    ldx     $FF02           ; Acknowledge the VSYNC interrupt
    ;
    lda     #%00010000      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     $FF9A
    ;
    leax    keyColumnScans,pcr
    lda     #%11111111
    sta     $FF02           ; Disable all keyboard column strobes
    ;
    ldb     #%00000001      ; Start off reading column 0
@loop_keyscan
    comb
    stb     $FF02           ; Enable the next column
    lda     $FF00           ; Read the column
    coma                    ; Invert bits so 1=key down, 0=key up
    sta     ,x+             ; Store in next keyColumnScans element
    ;
    comb
    lslb                    ; Next column. Shift bits in b to the left
    bne     @loop_keyscan
    ;
    tst     shouldScrollRight,pcr
    beq     @doneScrollRight
    lda     horizontalScroll,pcr
    inca
    sta     horizontalScroll,pcr
    ora     #%10000000
    sta     $FF9F
@doneScrollRight
    tst     shouldScrollLeft,pcr
    beq     @doneScrollLeft
    lda     horizontalScroll,pcr
    deca
    sta     horizontalScroll,pcr
    ora     #%10000000
    sta     $FF9F
@doneScrollLeft
    clr     triggerScroll,pcr
@doneVsync
    lda     #%00000000      ; BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    sta     $FF9A
    rti

;***********************
WaitForKeyPressNoRom:
;***********************
    pshs    b,a
@waitForNoKey
    bsr     _IsKeyDownNoRom
    bne     @waitForNoKey
@waitForKey
    bsr     _IsKeyDownNoRom
    beq     @waitForKey
@waitForNoKeyAgain
    bsr     _IsKeyDownNoRom
    bne     @waitForNoKeyAgain
    ;
    puls    a,b,pc

;***********************
_IsKeyDownRom:
;***********************
    ;
    ; Make ROM call to see if key is pressed
    jsr     [$A000]
    cmpa    #0
    bhi     @keyPressed
    clrd                            ; Return 0 for no key down
    bra     @done
@keyPressed
    ldd     #1                      ; Return 1 for key down
@done
    rts

;***********************
_IsKeyDownNoRom:
;***********************
    pshs    x
    leax    keyColumnScans,pcr
    lda     #7
@checkScan
    tst     a,x
    bne     @keyDown
    tsta
    beq     @noKeyDown
    deca
    bra     @checkScan
@noKeyDown
    ldd     #0                      ; Return 0 for no key down
    bra     @done
@keyDown
    ldd     #1                      ; Return 1 for key down
@done
    puls    x,pc

;***********************
_SetSafeMmuScratchBlock:
;***********************
    pshs    x,b,a
    ldd     6,s
    orcc    #$50
    stb     safeMmuLogicalBlock,pcr
    ldx     #$0000
@loop
    tstb
    beq     @doneMultiply
    leax    $2000,x
    decb
    bra     @loop
@doneMultiply
    stx     safeMmuLogicalBlockAddress,pcr
    andcc   #$AF
    ;
    puls    a,b,x,pc

;***********************
_GetSafeMmuScratchBlock:
;***********************
    clra
    ldb     safeMmuLogicalBlock,pcr
    rts

;***********************
_GetSafeMmuScratchBlockAddress:
;***********************
    ldd     safeMmuLogicalBlockAddress,pcr
    rts

; > a = physical block # to map
; < a = originally mapped physical block #
mapPhysicalBlockToSafeMmuLogicalBlock:
    pshs    x,b
    ; Map the phsyical block in a to safeMmuLogicalBlock
    ldb     safeMmuLogicalBlock,pcr
    ldx     #MMU_BANK_REGISTERS_FIRST
    abx
    sta     ,x
    ; Get original physical block mapped to safeMmuLogicalBlock
    leax    _MMU,pcr
    abx
    lda     ,x
    ; Clean up and return
    puls    b,x,pc

;***********************
TriggerAndWaitForScroll:
;***********************
    inc     triggerScroll,pcr
@wait
    tst     triggerScroll,pcr
    bne     @wait
    rts

    endsection
