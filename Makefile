CMOC_COMPILE_SWITCHES = --coco --compile --intermediate
CMOC_LINK_SWITCHES = --coco --intermediate --org=2000 --limit=7800 -L/usr/local/share/cmoc/lib
CMOC_LIBRARY_SWITCHES = -lcmoc-crt-ecb -lcmoc-std-ecb

all: ../scroll.dsk

%.o: ../%.c ../%.h
	cmoc $(CMOC_COMPILE_SWITCHES) -o $@ $<

%.o: ../%.asm
	cmoc $(CMOC_COMPILE_SWITCHES) -o $@ $<

s.bin: scroll.o scroll_lib.o diskRoutines.o
	cmoc $(CMOC_LINK_SWITCHES) -o s.bin scroll.o scroll_lib.o diskRoutines.o $(CMOC_LIBRARY_SWITCHES)

test.bin: test.o
	cmoc $(CMOC_LINK_SWITCHES) -o test.bin test.o $(CMOC_LIBRARY_SWITCHES)

../scroll.dsk: s.bin ../s.bas test.bin ../A.A
ifeq ("$(wildcard ../scroll.dsk)","")
	perl -e 'print chr(255) x (35*18*256)' > scroll.dsk
endif
	../../../lst2cmt.exe /SYSTEM coco3h /OFFSET 247A /OVERWRITE ./scroll_lib.lst "D:\Emulators\Mame\comments\coco3h.cmt"
	writecocofile --binary ../scroll.dsk s.bin
	writecocofile --binary ../scroll.dsk test.bin
	writecocofile --ascii ../scroll.dsk ../A.A
	writecocofile --ascii ../scroll.dsk ../s.bas

clean:
	find . -type f \( -name "*.o" -o -name "*.bin" -o -name "*.map" -o -name "*.link" -o -name "*.lst" -o -name "*.s" \) -delete
