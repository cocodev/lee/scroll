#include "scroll.h"

word debugOffset = 2;
byte MMU[8] = { 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x3E, 0x3F };

int main()
{
    initCoCoSupport();
    if (initializeDiskIo() == FALSE)
    {
        storeDebugCountToStartOfDebugOutput();
        c_PrintString("Disk support not detected.");
        return -1;
    }
    asm("ldmd", "#1");
    setHighSpeed(TRUE);
    asm("lbsr", "Set80x24TextMode");
    c_PrintString("v1.0\r");

    waitForKeyPress(TRUE, "Roms in, starting takeover");

    disableInterrupts();
    asm("lbsr", "DisableInterruptSources");
    asm("lbsr", "SwapRomsOut");
    setupInterruptVectors();
    SetSafeMmuScratchBlock(_LOGICAL_8000_9FFF);
    enableInterrupts();

    waitForKeyPress(FALSE, "Roms Out, about to call tests");
    asm("lbsr", "PerformScrollTest");

    waitForKeyPress(FALSE, "Roms Out, done with tests");

    disableInterrupts();
    asm("lbsr", "SwapRomsBackIn");
    asm("lbsr", "RestoreInterruptSources");
    SetSafeMmuScratchBlock(_LOGICAL_4000_5FFF);
    enableInterrupts();

    asm("lbsr", "Set80x24TextMode");
    waitForKeyPress(TRUE, "Roms In, done with takeover");

    resetPalette(TRUE);
    setHighSpeed(FALSE);
    asm("ldmd", "#0");
    storeDebugCountToStartOfDebugOutput();

    return 0; // Everything should be restored, so a simple return should return to BASIC
    asm
    {
SwapRomsOut                         import
SwapRomsBackIn                      import
    }
}

void storePeekValueToDebugOutput(word peekAddress)
{
    byte originalPhysicalBlock = MMU[GetSafeMmuScratchBlock()];
    word logicalAddress = GetSafeMmuScratchBlockAddress() + 0x0F00;
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, GetSafeMmuScratchBlock());
    poke(logicalAddress + debugOffset++, peek(peekAddress));
    mapPhysicalToLogicalMemory(originalPhysicalBlock, GetSafeMmuScratchBlock());
    storeDebugCountToStartOfDebugOutput();
}

void storePeekWordValueToDebugOutput(word peekAddress)
{
    byte originalPhysicalBlock = MMU[GetSafeMmuScratchBlock()];
    word logicalAddress = GetSafeMmuScratchBlockAddress() + 0x0F00;
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, GetSafeMmuScratchBlock());
    pokeWord(logicalAddress + debugOffset, peekWord(peekAddress));
    debugOffset += 2;
    mapPhysicalToLogicalMemory(originalPhysicalBlock, GetSafeMmuScratchBlock());
    storeDebugCountToStartOfDebugOutput();
}

void storeByteToDebugOutput(byte value)
{
    storePeekValueToDebugOutput(&value);
}

void storeWordToDebugOutput(word value)
{
    storePeekWordValueToDebugOutput(&value);
}

void storeDebugCountToStartOfDebugOutput()
{
    byte originalPhysicalBlock = MMU[GetSafeMmuScratchBlock()];
    word logicalAddress = GetSafeMmuScratchBlockAddress() + 0x0F00;
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, GetSafeMmuScratchBlock());
    pokeWord(logicalAddress, debugOffset - 2);
    mapPhysicalToLogicalMemory(originalPhysicalBlock, GetSafeMmuScratchBlock());
}

void doDiskTest()
{
    disableInterrupts();
    asm("lbsr", "SwapRomsBackIn");
    enableInterrupts();

    waitForKeyPress(TRUE, "Roms In, about to do disk test");

    char data[10];
    asm
    {
        pragma  undefextern
        pragma  newsource
        pshs    u
        ;
        ;// Open the file for input
        leax    testFilename,pcr
        lbsr    openFileForRead
        bcs     @error
        ;
        ;// Read 5 bytes into data
        ldx     #5
        stx     bytesToRead         ;// Number of bytes to read
        puls    u
        leax    data
        pshs    u
        stx     bytesReadBuffer     ;// Address of buffer to read into
        lbsr    readBytesFromFileIntoBuffer
        bcs     @error
        ;// Success!
        clra
        sta     ,y                  ;// Terminate the string with '\0'
        ;
        ;// Close the file
        lbsr    closeAllFiles
        ;
        puls    u
        bra     @done
@error
        puls    u
        ldx     #$2545              ;%E
        stx     data
        ldx     #$5252              ;RR
        stx     2+data
        ldx     #$4F52              ;OR
        stx     4+data
        clra
        sta     6+data              ;\0
        bra     @done
@done
        pragma  oldsource
    }
    c_PrintString("[");
    c_PrintString(data);
    c_PrintString("]\r");
    waitForKeyPress(TRUE, "Roms In, done with disk test");

    disableInterrupts();
    asm("lbsr", "SwapRomsOut");
    enableInterrupts();

    return;
    asm
    {
testFilename    fcn 'A.A'
    }
}

void waitForKeyPress(BOOL useRoms, const char* prompt)
{
    if (prompt)
    {
        c_PrintString(prompt);
        c_PrintString("...\r");
    }
    else
    {
        c_PrintString("Press any key...");
    }
    
    BOOL (*isKeyDown)() = IsKeyDownNoRom;
    if (useRoms)
    {
        isKeyDown = IsKeyDownRom;
    }
    while (isKeyDown()) {}
    while (isKeyDown() == FALSE) {}
    while (isKeyDown()) {}
}

asm void setupInterruptVectors()
{
    asm {
        pshs    y,x
        ldx     #$0100
        lda     #$7E            ;// jmp
        leay    InterruptHandler,pcr
        ldb     6
@setVector
        sta     ,x+
        sty     ,x++
        decb
        bne     @setVector
        puls    x,y
    }
}
